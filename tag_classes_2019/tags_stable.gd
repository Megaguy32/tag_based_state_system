#01. tool

#02. class_name

#03. extends

#04. # docstring
"""
This code tries to follow the PEP 8 coding style, recommended by Godot documentation
	https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html

Creation date: 2020 April
Author: EthanLR (Megaguy32)
Description: pre- Concurrent State Machine implementation of tags class.
	tags is a simple dictionary of booleans


"""

#05. signals
#06. enums
#07. constants
#08. exported variables
#09. public variables
#10. private variables
#11. onready variables

#12. optional built-in virtual _init method
#13. built-in virtual _ready method
#14. remaining built-in virtual methods
#15. public methods
#16. private methods


# [< Comment date 2020 Apr 10 >] TODO: consider making this its own node in editor
class_name TagsStable
extends Object

var _tags: Dictionary
var _tags_unapplied: Dictionary

func _init(dict: Dictionary) -> void:
	_tags = dict.duplicate()
	_tags_unapplied = dict.duplicate()

func get_to_read() -> Dictionary:
	return _tags.duplicate()

func get_to_write() -> Dictionary:
	call_deferred("_apply")
	return _tags_unapplied
	
func _apply() -> void:
	_tags = _tags_unapplied.duplicate()
