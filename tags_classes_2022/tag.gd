class_name Tag

#Creation date: 2022-03-28
#Author: EthanLR (Megaguy32)
#Description: ***
#
#this code tries to follow PEP 8 coding style, recommended by Godot documentation (3.2)


# 05. signals
signal tag_value_changed(tag_value)
# 06. enums
enum types {REGULAR, OR, NOT}
# 07. constants

# 08. exported variables

# - - - end section - - -

# 09. public variables
var name
var value
var type = ""
var dependees = [] # array of Tag
var dependers = [] # array of Tag

#var dependees_transitive # tags that this tag requires when triggered
#var dependers_transitive # tags that this tag impacts when triggered
# - - - end section - - -

# 10. private variables
# 11. onready variables
#
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method
# 14. remaining built-in virtual methods

func _init(name, value, type):
	assert(type in ["regular", "not", "or"])  # recognised type required
	self.name = name
	self.value = value
	self.type = type


func _to_string():
	var dep_string = ""
	for i in dependees:
		dep_string += "- %s\n" % i.name
		
	
	var the_dict = """(Tag Object)
name: %s
value: %s
type: %s
dependees: 
%s""" % [name,value,type,dep_string]
	
	return the_dict


# 15. public methods

func set_tag_value(tag_value):
	#if values are as they should be
	value = tag_value
	emit_signal("tag_value_changed", tag_value)
	
func operate():
	pass


# 16. private methods
