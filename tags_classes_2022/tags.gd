#01. tool

#02. class_name
class_name TagGroup

#03. extends
extends Resource

#04. # docstring
#This code tries to follow the PEP 8 coding style, recommended by Godot documentation
#	https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html
#
#Creation date: 2022-03-25
#Author: EthanLR (Megaguy32)
#Description: ***

#05. signals
#06. enums
enum rules {BECOME_TRUE, BECOME_FALSE, NOT_APPLICABLE, MIGHT_TRUE, MIGHT_FALSE, MIGHT}

#07. constants
const DEPENDEE = 0
const DEPENDER = 1

#08. exported variables
#09. public variables
var tags: Array
var edges: Array
var impaction_queue: Array

#10. private variables
#11. onready variables
#12. optional built-in virtual _init method
#13. built-in virtual _ready method
#14. remaining built-in virtual methods
func _to_string():
	var the_string: String = """(TagGroup Object)\n"""
	for i in tags:
		the_string += "%s\n" % str(i).indent("    ")
	return the_string

#15. public methods


func generate_impaction_queue_from_tag(tag_root: Tag) -> Array:
	# span undirected vertex graph from root node, creating queue of impacts in order of ripple effect
	
	print("\ngenerating impaction queue from root tag %s\n===" % tag_root.name)
	
	impaction_queue = []
	
#	var handled_impacters := [tag_root]
	var impacter_queue := [tag_root]
	
	for impacter in impacter_queue:
		
#		print("\ncurrent impacter: %s".indent("    ") % impacter.name)
		
		var numDependees: int = impacter.dependees.size()
		var c: int = -1
		
		for impactee in impacter.dependees + impacter.dependers:
			if impactee == tag_root: continue  # root tag is not impacted - no paradox pls
#			print("current impactee: %s".indent("    ") % impactee.name)
			
			c += 1
#			if impactee in impacter_queue: continue
			# dep is from perspective of impacter (what is impacter) so the opposite depency of the impactee
			var dep = DEPENDER if c < numDependees else DEPENDEE
			var impaction = Impaction.new(impacter, dep, impactee)
#			print("link %s <-> %s".indent("    ") % [impacter.name, impactee.name])
			impaction_queue.append(impaction)
			if impactee not in impacter_queue:
				impacter_queue.append(impactee)
			
#		handled_impacters.append(impacter)
		
	
	var report := "impaction_queue: [\n"
	for j in impaction_queue:
		report += "%s <-> %s,\n".indent("   ") % [j.impacter.name, j.impactee.name]
	report += "]"
	print_debug(report)
	
	return impaction_queue


func set_tag_value(tag_target, the_value: bool):
	"""
	View my documentation 'Tag traversal' to get a high level understanding of 
	whats happening here.
	
	when you change a single tag value, it has a ripple effect on its 
	neighbouring tags (dependencies), this code calculates what the dependencies
	become.
	
	There is a queue of tags that are dealt with, and if a tag's value is
	uncertain, then it is dealt with last
	"""
	
	print_debug("\nTags setting tag: " + tag_target.name + ", to: " + str(the_value))
	
#	tag_target.set_tag_value(the_value)  # no recursion
#	print_debug("Target tag set")
	# it deal with the uncertain values now, add them to the queue
#		while queue_query != []:
#			var tag_query = queue_query.pop_front()
#			tag_query.operate()
#			queue_tag.append(tag_query)
			
			
	# 2022 >>>
		
	tag_target.value = the_value
	
	var impaction_queue: Array = generate_impaction_queue_from_tag(tag_target)
	
	# initialize impaction_queue values
	for i in impaction_queue:
		if i.impacter != tag_target: break  # first impacters are tag_target
		i.impacter_val = the_value
	
	while !impaction_queue.is_empty():
		
		var impaction = impaction_queue.pop_front()
		
#		if !impaction.impacter_val is bool: 
#			print_debug("""
#				⚠️ Yo Yo Yo my name is Slimey D. ✌️  - I speak for the code
#				And you seem to have got yoself a ==="Bad Impaction"===
#				All Impactions have the right to a impacter val, c'mon man...
#				""")
#			print_debug(impaction)
#			assert(false)
		
		print_debug("\nCurrent Impaction\n===\n\n" + str(impaction).indent("    "))
		
		var rule = find_traversal_rule(
			impaction.impacter_val, impaction.impacter, impaction.dep, impaction.impactee
			)
		apply_traversal_rule(rule, impaction)


func find_traversal_rule(val: bool, impacter: Tag, dep: int, impactee: Tag) -> int:
	# report is one of these constants (BECOME_TRUE, BECOME_FALSE, NOT_APPLICABLE, MIGHT_TRUE, MIGHT_FALSE)
	
	# dep is from impacter tag's perspective - what is the impacter
	
	print_debug("finding traversal rule for current impaction".indent("    "))
	
	match [val, impacter.type, dep, impactee.type]:
		[null, ..]:
			print_debug("?⊛...  MIGHT".indent("    "))
			return rules.MIGHT
		
		# becomes true	T ○ ⟶ ⊛
		[true, "regular", DEPENDER, _]:
			print_debug("T ○ ⟶ ⊛  BECOME_TRUE".indent("    "))
			return rules.BECOME_TRUE
		
		# becomes true	T ⊛ ⟵ ⊚
		[true, _, DEPENDEE, "or"]:
			print_debug("T ⊛ ⟵ ⊚")
			print_debug("BECOME_TRUE".indent("    "))
			return rules.BECOME_TRUE
		
		# becomes false	T ⊖ ⟶ ⊛
		[true, "not", DEPENDER, _]:
			print_debug("T ⊖ ⟶ ⊛")
			print_debug("BECOME_FALSE".indent("    "))
			return rules.BECOME_FALSE
		
		# becomes false	F ⊚ ⟶ ⊛
		[false, "or", DEPENDER, _]:
			print_debug("F ⊚ ⟶ ⊛".indent("    "))
			print_debug("BECOME_FALSE".indent("    "))
			return rules.BECOME_FALSE

		# becomes false	F ⊛ ⟵ ○
		[false, _, DEPENDEE, "regular"]:
			print_debug("F ⊛ ⟵ ○".indent("    "))
			print_debug("BECOME_FALSE".indent("    "))
			return rules.BECOME_FALSE

		# becomes false	T ⊛ ⟵ ⊖
		[true, _, DEPENDEE, "not"]:
			print_debug("T ⊛ ⟵ ⊖".indent("    "))
			print_debug("BECOME_FALSE".indent("    "))
			return rules.BECOME_FALSE

		# Not applicable	F ○ ⟶ ⊛
		[false, "regular", DEPENDER, _]:
			print_debug("F ○ ⟶ ⊛".indent("    "))
			print_debug("NA".indent("    "))
			return rules.NOT_APPLICABLE

		# Not applicable	T ⊛ ⟵ ○
		[true, _, DEPENDEE, "regular"]:
			print_debug("T ⊛ ⟵ ○".indent("    "))
			print_debug("NA".indent("    "))
			return rules.NOT_APPLICABLE

		# might true	T ⊚ ⟶ ⊛
		[true, "or", DEPENDER, _]:
			print_debug("T ⊚ ⟶ ⊛".indent("    "))
			print_debug("MIGHT True".indent("    "))
			return rules.MIGHT_TRUE

		# might true	F ⊖ ⟶ ⊛
		[false, "not", DEPENDER, _]:
			print_debug("F ⊖ ⟶ ⊛".indent("    "))
			print_debug("MIGHT True".indent("    "))
			return rules.MIGHT_TRUE

		# might true	F ⊛ ⟵ ⊖
		[false, _, DEPENDEE, "not"]:
			print_debug("F ⊛ ⟵ ⊖".indent("    "))
			print_debug("MIGHT True".indent("    "))
			return rules.MIGHT_TRUE

		# might false	F ⊛ ⟵ ⊚
		[false, _, DEPENDEE, "or"]:
			print_debug("F ⊛ ⟵ ⊚".indent("    "))
			print_debug("MIGHT False".indent("    "))
			return rules.MIGHT_FALSE

	# if none of those work then there is a case I havent accounted for
	var print_val = ""
	print_val += "ERROR, unhandled tag traversal rule case"
	print_val += "\nvalue: " + str(val)
	print_val += "\nimpacter tag:\n" + str(impacter).indent("    ")
	print_val += "\nimpacter type: " + impacter.type
	print_val += "\nDep (0-Dependee, 1-Depender): " + str(dep)
	print_val += "\nimpactee tag:\n" + str(impactee).indent("    ")
	print_val += "\nimpactee type: " + impactee.type
	
	print_debug(print_val.indent("    "))
	assert(false) # perposfully crash the game
	return 0 # unnecessary return value to satisfy linting


func apply_traversal_rule(rule: int, impaction: Impaction):
	
	var current_impactee: Tag = impaction.impactee
	
	print_debug("applying rule for current impaction".indent("    "))
	
	match rule:
		rules.BECOME_TRUE:
			print("impaction result: tag ".indent("    ") + current_impactee.name + " becomes true")
			current_impactee.value = true  # no recursion
			tag_rule_apply_become(impaction, current_impactee)
			
		rules.BECOME_FALSE:
			print("impaction result: tag ".indent("    ") + current_impactee.name + " becomes true")
			current_impactee.value = false  # no recursion
			tag_rule_apply_become(impaction, current_impactee)
			
		rules.NOT_APPLICABLE:
			impaction.state = impaction.states.NOT_APPLICABLE
			pass
			
		rules.MIGHT_TRUE, rules.MIGHT_FALSE, rules.MIGHT:
			# TODO the code is good, but ignoring for now, because OR code is yet unimplemented
			
#			# new mystery
#			if current_impaction.state == current_impaction.states.UNCHECKED:
#				current_impaction.state = current_impaction.states.MIGHT
#				impaction_queue.push_back(current_impaction)  # back of the line!
#				print_debug("push back")
#
#			# at wits end
#			if current_impaction.state == current_impaction.states.MIGHT:
#				print_debug("WATTE??")
#				print_debug("""
#				⚠️ Yo Yo Yo my name is Slimey D. ✌️ 
#				And you seem to have got yoself a "Unsatisfied Tag Operator"
#				Tags deserve better code lol
#				""")
##				print_debug(current_impaction)
#				assert(false)
			
			pass


func tag_rule_apply_become(current_impaction, current_impactee):
	print_debug("impaction result: tag " + current_impactee.name + " becomes true / false".indent("    "))
			
	current_impaction.state = current_impaction.states.BECAME
	
	# Consequences of become case
	print("Checking concequences of BECOME:".indent("    "))
	
	
	var impaction_queue_duplicate: Array = impaction_queue.duplicate()
	while !impaction_queue_duplicate.is_empty():
		var impaction: Impaction = impaction_queue_duplicate.pop_front()
		var impacter: Tag = impaction.impacter
		var impactee: Tag = impaction.impactee
#		print("check %s <-> %s".indent("    ") % [impacter.name, impactee.name])
		
		# re-mystify remaining impactions
		impaction.state = impaction.states.UNCHECKED
		
		# impactee now has value value to impact others
		if impacter == current_impactee:
#			print("set impacter val".indent("    "))
			impaction.impacter_val = current_impactee.value
			assert(impaction.impacter_val is bool)
			
		# remove redundant impactions impacting current_impactee
		if impactee == current_impactee:
			print("ERASE %s <-> %s".indent("    ") % [impacter.name, impactee.name])
			impaction_queue.erase(impaction)
			impaction_queue_duplicate.erase(impaction)
			


func connect_tags(tag_depender, tag_dependee):
	assert(tag_depender in tags)
	assert(tag_dependee in tags)
	tag_depender.dependees.append(tag_dependee)
	tag_dependee.dependers.append(tag_depender)
	

#16. private methods
	
	
class Impaction:
				
	var impacter_val:
		get: 
			assert(impacter_val is bool)
			return impacter_val
	var impacter: Tag
	var dep: int
	var impactee: Tag
	
	enum states {UNCHECKED, MIGHT, BECAME, NOT_APPLICABLE}
	var state = states.UNCHECKED
	
	func _init(impacter: Tag, dep: int, impactee: Tag):
		self.impacter = impacter 
		self.dep = dep
		self.impactee = impactee
		
	func _to_string():
		var print_val = "(Impaction Object)"
		print_val += "\nimpacter value: " + str(impacter_val)
		print_val += "\nimpacter:\n" + str(impacter).indent("    ")
		print_val += "\ndep (0: dependee, 1: depender):" + str(dep)
		print_val += "\nimpactee:\n" + str(impactee).indent("    ")
		print_val += "\n"
		
		return print_val
			
			
