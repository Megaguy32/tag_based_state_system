#01. tool
#02. class_name


#03. extends
extends Object

#04. # docstring
#This code tries to follow the PEP 8 coding style, recommended by Godot documentation
#	https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html
#
#Creation date: ***
#Author: EthanLR (Megaguy32)
#Description: ***

#05. signals
#06. enums
#07. constants
#08. exported variables
#09. public variables
var tag_group_dict: Dictionary
#10. private variables
#11. onready variables

#12. optional built-in virtual _init method
func run():
	var tag_group := import_the_thing()
	
	
func open_file(filepath: String):
	
	var file = FileAccess.open(filepath, FileAccess.READ)
	var content = file.get_as_text()
	file.close()
	return content
	
	
func import_the_thing() -> TagGroup:
	
	print_debug("Running Tag Import Script")
	
	var the_string = open_file("tags.json")
	
	# Extract JSON
	print("JSON string to parse:\n%s" % the_string.indent("    "))
	var json:JSON = JSON.new()
	json.parse(the_string)
	tag_group_dict = json.get_data()
	
	print("JSON dictionary result: \n%s" % str(tag_group_dict).indent("    "))
	
	# now mak the tag group
	return makTegs()
	
	
func makTegs():
	var tag_group := TagGroup.new()
	var tags_cached := {}
	
	# Initialize tags
	for k in tag_group_dict:
		var tag: Tag
		var name = k
		var value = tag_group_dict[k].value
		var type = tag_group_dict[k].type
		
		tag = Tag.new(name, value, type)
			
		tags_cached[k] = tag
	
	# Add tags to tag group
	tag_group.tags = tags_cached.values()
	
	# Add dependees once all tags initialized
	print_debug("Adding dependees since all tags declared")
	for depender in tag_group_dict:
		for dependee in tag_group_dict[depender].dependees:
			tag_group.connect_tags(tags_cached[depender], tags_cached[dependee])
	
	print("\nTagGroup import complete, result:\n===\n")
	print(tag_group)
	
	print("--- Tag Import Script Complete ---")
	
	return tag_group


#13. built-in virtual _ready method
#14. remaining built-in virtual methods
#15. public methods
#16. private methods
